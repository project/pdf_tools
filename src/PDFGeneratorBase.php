<?php

namespace Drupal\pdf_tools;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class PDFGeneratorBase implements PDFGeneratorInterface {

  /**
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

 /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Render\BareHtmlPageRenderer
   */
  protected $bareHtmlPageRenderer;

  /**
   * @var \Drupal\pdf_tools\PDFManipulatorInterface
   */
  protected $pdfManipulator;

  /**
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $theme;

  /**
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected $themeInitialization;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * PDFGeneratorBase constructor.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Render\BareHtmlPageRendererInterface $bare_html_page_renderer
   * @param \Drupal\pdf_tools\PDFManipulatorInterface $pdf_manipulator
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   */
  public function __construct(
    FileSystem $file_system,
    EntityTypeManagerInterface $entity_type_manager,
    BareHtmlPageRendererInterface $bare_html_page_renderer,
    PDFManipulatorInterface $pdf_manipulator,
    ThemeManagerInterface $theme_manager,
    ThemeInitializationInterface $theme_initialization,
    RequestStack $request_stack
  ) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->bareHtmlPageRenderer = $bare_html_page_renderer;
    $this->pdfManipulator = $pdf_manipulator;
    $this->theme = $theme_manager;
    $this->requestStack = $request_stack;
    $this->themeInitialization = $theme_initialization;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function entityToPDF(EntityInterface $entity, $display = [], array $options = []) {
    $build = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId())->view($entity, $display);
    $build['#pdf_title'] = $entity->label();

    return $this->renderArrayToPDF($build, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function renderArrayToPDF(array $elements, array $options = []) {
    $style = new PDFGenerationStyle([], $options);
    $content = $this->renderElements($elements, $style);
    return $this->generateFromHTML($content, $options);
  }

  /**
   * Convert a render array to markup for PDF generation.
   *
   * @param array $elements
   *   The render array.
   * @param \Drupal\pdf_tools\PDFGenerationStyle $style
   *   The style options for the generation.
   *
   * @return string
   *   The rendered markup.
   */
  protected function renderElements(array $elements, PDFGenerationStyle $style): string {
    if ($theme = $style->getOption('theme')) {
      $this->theme->setActiveTheme(
        $this->themeInitialization->initTheme($theme)
      );
    }

    $response = $this->bareHtmlPageRenderer->renderBarePage(
      $elements,
      !empty($elements['#pdf_title']) ? $elements['#pdf_title'] : 'No Title',
      'pdf',
      ['#show_messages' => FALSE]
    );

    if ($theme) {
      $this->theme->resetActiveTheme();
    }

    return $response->getContent();
  }

  /**
   * Convert relative URLs to absolute URLs suitable for PDF generation.
   *
   * @param string $content
   *   The markup with relative URLs.
   *
   * @return string
   *   The markup with absolute URLs.
   */
  protected function convertUrlsToAbsolute(string $content): string {
    return preg_replace(
      '#(src="|href=")/#i',
      '${1}' . $this->request->getSchemeAndHttpHost() . '/',
      $content
    );
  }

  /**
   * Get the outfile from the options.
   *
   * @param array $options
   *   If the key '__destination' is set this is used, otherwise a temporary file
   *   is created.
   *
   * @return string
   *   The file uri of the output file.
   */
  protected function getOutFile(array $options = array()) {
    if (isset($options['__destination'])) {
      $out_file = $options['__destination'];

      $directory = $this->fileSystem->dirname($out_file);
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $out_file = $this->fileSystem->getDestinationFilename($out_file, FileSystemInterface::EXISTS_RENAME);
    }
    else {
      $out_file = $this->tempnamWithExtension('pdf', 'pdfgen_');
    }

    return $out_file;
  }

  /**
   * Generate a tempory file with a given extension.
   *
   * @param $ext
   * @param $prefix
   * @param string $directory
   *
   * @return bool|string
   */
  protected function tempnamWithExtension($ext, $prefix, $directory = 'temporary://') {
    do {
      $tmp_file = $this->fileSystem->tempnam($directory, $prefix);
    } while (!rename($tmp_file, $tmp_file.'.'.$ext));
    $tmp_file .= '.'.$ext;

    return $tmp_file;
  }
}
