<?php

namespace Drupal\pdf_tools;

use Drupal\Core\File\FileSystemInterface;

class PDFTKPDFManipulator implements PDFManipulatorInterface {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Drupal\pdf_tools\PDFAnalyserInterface
   */
  protected $pdfAnalyser;

  /**
   * PDFTKPDFManipulator constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   */
  public function __construct(
    FileSystemInterface $file_system,
    PDFAnalyserInterface $pdf_analyser
  ) {
    $this->fileSystem = $file_system;
    $this->pdfAnalyser = $pdf_analyser;
  }

  /**
   * Stamp the stamp pdf onto all pages of the input pdf.
   *
   * @param string $pdf
   *   The URI of the pdf to have to the stamp applied to.
   * @param string $stamp_pdf
   *   The URI of the pdf to be stamped onto the input pdf.
   * @param string $output_pdf
   *   (Optional) The URI of the destination pdf.
   *
   * @return string
   *   The URI of the updated pdf.
   */
  public function stamp($pdf, $stamp_pdf, $output_pdf = NULL) {
    if ($output_pdf) {
      $directory = $this->fileSystem->dirname($output_pdf);
      $this->fileSystem->prepareDirectory(
        $directory,
        FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY
      );
    }
    else {
      $directory = 'temporary://pdf_tools/stamped/';
      $this->fileSystem->prepareDirectory(
        $directory,
        FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY
      );
      $output_pdf = tempnam($directory, 'stamped');
      $this->fileSystem->move($output_pdf, $output_pdf.'.pdf');
      $output_pdf = $output_pdf.'.pdf';
    }

    // If the stamp pdf has multiple pages then we use multistamp instead of
    // stamp
    $operation = 'stamp';
    if ($this->pdfAnalyser->countPages($stamp_pdf) > 1) {
      $operation = 'multistamp';
    }

    $script = 'pdftk '.
      $this->fileSystem->realpath($pdf).' '.$operation.' '.
      $this->fileSystem->realpath($stamp_pdf).' output '.
      $this->fileSystem->realpath($output_pdf);
    exec($script);

    if (file_exists($output_pdf) && filesize($output_pdf)) {
      return $output_pdf;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Stamp the stamp pdf onto all pages of the input pdf.
   *
   * @param string $pdf
   *   The URI of the pdf to have to the stamp applied to.
   * @param string $background_pdf
   *   The URI of the pdf to be stamped onto the input pdf.
   * @param string $output_pdf
   *   (Optional) The URI of the destination pdf.
   *
   * @return string
   *   The URI of the updated pdf.
   */
  public function background($pdf, $background_pdf, $output_pdf = NULL) {
    if ($output_pdf) {
      $directory = $this->fileSystem->dirname($output_pdf);
      $this->fileSystem->prepareDirectory(
        $directory,
        FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY
      );
    }
    else {
      $directory = 'temporary://pdf_tools/backgrounded/';
      $this->fileSystem->prepareDirectory(
        $directory,
        FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY
      );
      $output_pdf = tempnam($directory, 'backgrounded');
      $this->fileSystem->move($output_pdf, $output_pdf.'.pdf');
      $output_pdf = $output_pdf.'.pdf';
    }

    // If the background pdf has multiple pages then we use multibackground instead of
    // background
    $operation = 'background';
    if ($this->pdfAnalyser->countPages($background_pdf) > 1) {
      $operation = 'multibackground';
    }

    $script = 'pdftk '.
      $this->fileSystem->realpath($pdf).' '.$operation.' '.
      $this->fileSystem->realpath($background_pdf).' output '.
      $this->fileSystem->realpath($output_pdf);
    exec($script);

    if (file_exists($output_pdf) && filesize($output_pdf)) {
      return $output_pdf;
    }
    else {
      return FALSE;
    }
  }
}
