<?php

namespace Drupal\pdf_tools\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class PDFStyleAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pdf_tools\Entity\PDFStyle $entity */
    $access = parent::checkAccess($entity, $operation, $account);

    if ($operation === 'view') {
      $view_access = AccessResult::allowedIf($entity->visibility->value === 'public')
        ->orIf(AccessResult::allowedIf($entity->getOwnerId() === $account->id()))
        ->addCacheableDependency($entity)
        ->cachePerUser();

      $access = $access->orIf($view_access);
    }
    else if (in_array($operation, ['edit', 'update'])) {
      $edit_access = AccessResult::allowedIf($entity->getOwnerId() === $account->id())
        ->andIf(AccessResult::allowedIfHasPermission($account, 'update own pdf styles'))
        ->cachePerUser();

      $access = $access->orIf($edit_access);
    }
    else if (in_array($operation, ['duplicate', 'delete'], TRUE)) {
      $operation_access = AccessResult::allowedIf($entity->getOwnerId() === $account->id())
        ->andIf(AccessResult::allowedIfHasPermission($account, $operation . ' own pdf styles'))
        ->cachePerUser();

      $access = $access->orIf($operation_access);
    }

    return $access;
  }

}
