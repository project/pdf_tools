<?php

namespace Drupal\pdf_tools\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Class PDFStyle
 *
 * @ContentEntityType(
 *   id = "pdf_style",
 *   label = @Translation("PDF Style"),
 *   label_singular = @Translation("pdf style"),
 *   label_plural = @Translation("pdf styles"),
 *   label_count = @PluralTranslation(
 *     singular = "@count pdf style",
 *     plural = "@count pdf styles"
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "access" = "Drupal\pdf_tools\Entity\PDFStyleAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\pdf_tools\Form\PDFStyleForm",
 *       "duplicate" = "Drupal\pdf_tools\Form\PDFStyleForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\pdf_tools\Entity\PDFStyleListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\pdf_tools\Routing\PDFStyleHtmlRouteProvider"
 *     }
 *   },
 *   base_table = "pdf_style",
 *   revision_table = "pdf_style_revision",
 *   admin_permission = "administer pdf styles",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "owner" = "owner",
 *     "label" = "label",
 *   },
 *   links = {
 *     "collection" = "/admin/config/pdf/style",
 *     "canonical" = "/admin/config/pdf/style/{pdf_style}",
 *     "edit-form" = "/admin/config/pdf/style/{pdf_style}/edit",
 *     "duplicate-form" = "/admin/config/pdf/style/{pdf_style}/duplicate",
 *     "delete-form" = "/admin/config/pdf/style/{pdf_style}/delete",
 *     "add-form" = "/admin/config/pdf/style/add"
 *   }
 * )
 *
 * @package Drupal\pdf_tools\Entity
 */
class PDFStyle extends ContentEntityBase implements EntityOwnerInterface {
  use EntityOwnerTrait;

  /**
   * Orientation Constants.
   */
  const ORIENTATION_PORT = 'Portrait';
  const ORIENTATION_LAND = 'Landscape';

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Label'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -100,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['theme'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Theme'))
      ->setDescription(new TranslatableMarkup('The theme to render the PDF content in.'))
      ->setSetting('allowed_values_function', [static::class, 'themeOptions'])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['visibility'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Access'))
      ->setSetting('allowed_values', [
        'public' => new TranslatableMarkup('Public'),
        'private' => new TranslatableMarkup('Private'),
      ])
      ->setDefaultValue([['value' => 'public']])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['background_pdf'] = BaseFieldDefinition::create('file')
      ->setLabel(new TranslatableMarkup('Background PDF'))
      ->setSetting('uri_scheme', 'private')
      ->setSetting('file_extensions', 'pdf')
      ->setSetting('file_directory', "pdf_tools/stamps")
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['stamp_pdf'] = BaseFieldDefinition::create('file')
      ->setLabel(new TranslatableMarkup('Overlay PDF'))
      ->setSetting('uri_scheme', 'private')
      ->setSetting('file_extensions', 'pdf')
      ->setSetting('file_directory', "pdf_tools/stamps")
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['page_size'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Page Size'))
      ->setSetting('allowed_values_function', [static::class, 'pageSizeOptions'])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['page_width'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Page Width'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'settings' => [
          'size' => 10,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['page_height'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Page Height'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'settings' => [
          'size' => 10,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['orientation'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Page Orientation'))
      ->setSetting('allowed_values', [
        static::ORIENTATION_LAND => new TranslatableMarkup('Landscape'),
        static::ORIENTATION_PORT => new TranslatableMarkup('Portrait'),
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('view', TRUE);

    foreach (['bottom', 'left', 'right', 'top'] as $edge) {
      $fields['margin_'.$edge] = BaseFieldDefinition::create('string')
        ->setLabel(new TranslatableMarkup(ucfirst($edge).' Margin'))
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'settings' => [
            'size' => 10,
          ],
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    }

    $fields['outline'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Include Outline'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['outline_depth'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Outline Depth'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    // @todo: Consider include in outline and exclude from outline.

    $fields['background'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Include Background'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('view', TRUE);

    // Whether or not to use forms.
    // WKHtmlToPDF: This maps to the --enable-forms --disable-forms options.
    $fields['forms'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Use PDF Forms?'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('view', TRUE);

    // @todo: Page Offset

    // Whether or not to use smart shrinking.
    // WKHtmlToPDF: This maps to the --disable-smart-shrinking,
    // --enable-smart-shrinking
    $fields['smart_shrinking'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Use smart shrinking?'))
      ->setDefaultValue([
        'value' => TRUE,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('view', TRUE);

    foreach (['header', 'footer'] as $hf) {
      $fields["{$hf}_type"] = BaseFieldDefinition::create('list_string')
        ->setLabel(new TranslatableMarkup(ucfirst($hf).' Type'))
        ->setSetting('allowed_values', [
          'html' => new TranslatableMarkup('HTML'),
          'file' => new TranslatableMarkup('File'),
          'plain' => new TranslatableMarkup('Plain'),
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      foreach (['left', 'center', 'right'] as $pos) {
        $fields["{$hf}_{$pos}"] = BaseFieldDefinition::create('string')
          ->setLabel(new TranslatableMarkup(ucfirst($hf).' '.ucfirst($pos)))
          ->setDisplayOptions('form', [
            'type' => 'string_textfield',
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);
      }

      $fields["{$hf}_font_name"] = BaseFieldDefinition::create('string')
        ->setLabel(new TranslatableMarkup(ucfirst($hf).' Font Name'))
        ->setDefaultValue([
          'value' => 'Arial',
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields["{$hf}_font_size"] = BaseFieldDefinition::create('string')
        ->setLabel(new TranslatableMarkup(ucfirst($hf).' Font Size'))
        ->setDefaultValue([
          'value' => '14',
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'settings' => [
            'size' => 5,
          ]
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields["{$hf}_html"] = BaseFieldDefinition::create('text_long')
        ->setLabel(new TranslatableMarkup(ucfirst($hf). ' Content'))
        ->setDisplayOptions('form', [
          'type' => 'text_textarea',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields["{$hf}_file"] = BaseFieldDefinition::create('file')
        ->setLabel(new TranslatableMarkup(ucfirst($hf). ' File'))
        ->setSetting('uri_scheme', 'private')
        ->setSetting('file_extensions', 'html htm')
        ->setSetting('file_directory', "pdf_tools/{$hf}s")
        ->setDisplayOptions('form', [
          'type' => 'file_generic',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields["{$hf}_line"] = BaseFieldDefinition::create('boolean')
        ->setLabel(new TranslatableMarkup('Display '.ucfirst($hf). ' Line?'))
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields["{$hf}_spacing"] = BaseFieldDefinition::create('integer')
        ->setLabel(new TranslatableMarkup(ucfirst($hf).' Spacing'))
        ->setSetting('suffix', 'mm')
        ->setDisplayOptions('form', [
          'type' => 'number',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    }

    $fields['grayscale'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Grayscale'))
      ->setDescription(new TranslatableMarkup('PDF will be generated in grayscale'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['dpi'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('DPI'))
      ->setDescription(new TranslatableMarkup('Set the DPI explicitly'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Get the page size options.
   *
   * @return array
   */
  public static function pageSizeOptions() {
    return [
      'A9' => 'A9',
      'A8' => 'A8',
      'A7' => 'A7',
      'A6' => 'A6',
      'A5' => 'A5',
      'A4' => 'A4',
      'A3' => 'A3',
      'A2' => 'A2',
      'A1' => 'A1',
      'A0' => 'A0',
      'Executive' => 'Executive 7.5 x 10',
      'Legal' => 'Legal 8.5 x 14',
      'Letter' => 'Letter 8.5 x 11',
      'Custom' => 'Custom',
    ];
  }

  /**
   * Get the theme options.
   */
  public static function themeOptions() {
    /** @var \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler */
    $theme_handler = \Drupal::service('theme_handler');
    $themes = $theme_handler->listInfo();

    $options = [];
    foreach ($themes as $name => $theme) {
      if (empty($theme->info['hidden'])) {
        $options[$name] = $theme->info['name'];
      }
    }
    return $options;
  }

}
