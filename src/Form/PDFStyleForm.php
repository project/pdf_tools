<?php

namespace Drupal\pdf_tools\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\pdf_tools\PDFGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class PDFStyleForm extends ContentEntityForm {

  /**
   * @var \Drupal\pdf_tools\PDFGeneratorInterface
   */
  protected $pdfGenerator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pdf_tools.generator'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  public function __construct(
    PDFGeneratorInterface $pdf_generator,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->pdfGenerator = $pdf_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    if ($this->operation === 'duplicate') {
      $entity = $entity->createDuplicate();
    }

    return parent::setEntity($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['preview'] = [
      '#type' => 'submit',
      '#value' => $this->t('Preview'),
      '#submit' => ['::submitForm', '::downloadPreview'],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['page'] = [
      '#type' => 'details',
      '#weight' => -10,
      '#open' => TRUE,
      '#title' => new TranslatableMarkup('Page'),
      '#description' => new TranslatableMarkup('Page Size & Orientation Settings'),
    ];
    foreach ([
      'page_size', 'page_width', 'page_height', 'orientation', 'margin_top',
      'margin_left', 'margin_right', 'margin_bottom'
             ] as $page_field) {
      $form['page'][$page_field] = $form[$page_field];
      unset($form[$page_field]);
    }

    $form['rendering'] = [
      '#type' => 'details',
      '#weight' => -8,
      '#open' => TRUE,
      '#title' => new TranslatableMarkup('Rendering'),
      '#description' => new TranslatableMarkup('Setting related to rendering the pdf itself.')
    ];
    foreach (['theme', 'forms', 'background', 'smart_shrinking', 'background_pdf', 'stamp_pdf'] as $rendering_field) {
      $form['rendering'][$rendering_field] = $form[$rendering_field];
      unset($form[$rendering_field]);
    }

    $form['header'] = [
      '#type' => 'details',
      '#weight' => -5,
      '#open' => TRUE,
      '#title' => new TranslatableMarkup('Headers'),
      '#description' => new TranslatableMarkup('Header settings applied to every page'),
    ];
    $form['footer'] = [
      '#type' => 'details',
      '#weight' => -2,
      '#open' => TRUE,
      '#title' => new TranslatableMarkup('Footers'),
      '#description' => new TranslatableMarkup('Footer settings applied to every page'),
    ];
    foreach (Element::children($form) as $child) {
      if (strpos($child, 'header_') === 0) {
        $form['header'][$child] = $form[$child];

        if (!in_array($child, ['header_type', 'header_line', 'header_spacing'])) {
          $plain_fields = [
            'header_right', 'header_center', 'header_left', 'header_font_size',
            'header_font_name'
          ];
          if (in_array($child, $plain_fields)) {
            $value = 'plain';
          }
          else if ($child == 'header_file') {
            $value = 'file';
          }
          else if ($child == 'header_html') {
            $value = 'html';
          }

          $form['header'][$child]['#states'] = [
            'visible' => [
              'select[name="header_type"]' => ['value' => $value]
            ]
          ];
        }
        unset($form[$child]);
      }
      else if (strpos($child, 'footer_') === 0) {
        $form['footer'][$child] = $form[$child];

        if (!in_array($child, ['footer_type', 'footer_line', 'footer_spacing'])) {
          $plain_fields = [
            'footer_right', 'footer_center', 'footer_left', 'footer_font_size',
            'footer_font_name'
          ];
          if (in_array($child, $plain_fields)) {
            $value = 'plain';
          }
          else if ($child == 'footer_file') {
            $value = 'file';
          }
          else if ($child == 'footer_html') {
            $value = 'html';
          }

          $form['footer'][$child]['#states'] = [
            'visible' => [
              'select[name="footer_type"]' => ['value' => $value]
            ]
          ];
        }

        unset($form[$child]);
      }
    }

    return $form;
  }

  public function downloadPreview(array $form, FormStateInterface $form_state) {
    $uri = $this->pdfGenerator->renderArrayToPDF(
      $this->getPdfSampleContent(),
      [
        'pdf_style' => $this->entity,
      ]
    );

    $form_state->setResponse(new BinaryFileResponse(
      $uri,
      200,
      [],
      true,
      ResponseHeaderBag::DISPOSITION_ATTACHMENT
    ));
  }

  /**
   * Get a sample render array.
   */
  protected function getPdfSampleContent() {
    $elements = [];

    $elements['p1'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet iaculis purus. Nunc lacinia orci maximus purus tincidunt, nec condimentum sapien commodo. Nulla venenatis dapibus dui nec volutpat. Integer pharetra purus non mi commodo, nec lobortis metus mollis. Praesent sit amet condimentum leo. Phasellus a magna finibus, placerat lacus sed, tempor augue. Proin a ornare neque. Praesent mauris eros, ultrices ut enim et, condimentum blandit urna. In eu mi quis ipsum aliquam auctor at vitae mi. Praesent faucibus vitae lacus non accumsan.',
    ];
    $elements['p2'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Etiam a purus varius, congue felis laoreet, tempor neque. Curabitur dictum malesuada ultricies. Proin rhoncus sodales libero, eget dignissim magna rutrum nec. Sed imperdiet mattis venenatis. Curabitur eget pretium leo. Duis felis massa, viverra et semper at, gravida eu ligula. Donec id risus mattis, fringilla velit nec, luctus quam. Ut ultricies velit vitae convallis tempus.',
    ];
    $elements['ulist'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        'Vivamus scelerisque arcu sodales, tempus urna id, ullamcorper neque.',
        'Aliquam facilisis nulla in lacus sollicitudin placerat.',
        'Mauris a ex tempor, condimentum turpis eu, mattis nunc.',
        'Morbi ut nulla sed dui maximus dignissim non eu nunc.',
        'Sed ut nunc malesuada, tristique arcu rutrum, scelerisque justo.',
      ],
    ];
    $elements['olist'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ol',
      '#items' => [
        'Vivamus scelerisque arcu sodales, tempus urna id, ullamcorper neque.',
        'Aliquam facilisis nulla in lacus sollicitudin placerat.',
        'Mauris a ex tempor, condimentum turpis eu, mattis nunc.',
        'Morbi ut nulla sed dui maximus dignissim non eu nunc.',
        'Sed ut nunc malesuada, tristique arcu rutrum, scelerisque justo.',
      ],
    ];
    $elements['p3'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Sed sit amet mauris nunc. Donec auctor elementum odio. Fusce sit amet tincidunt augue. Donec finibus nisi non ligula iaculis fermentum. Suspendisse laoreet tempor mauris eget blandit. Donec sit amet posuere eros, ut commodo leo. Maecenas eu augue metus. Curabitur finibus ultrices velit, a bibendum sem volutpat sed. Donec sed placerat ipsum, vel iaculis diam. Curabitur sapien nisi, imperdiet ut hendrerit a, euismod egestas erat. Curabitur ullamcorper aliquam nibh at tincidunt. Integer orci risus, viverra at imperdiet eu, eleifend ut dui.',
    ];
    $elements['p4'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Cras nec velit quis diam sollicitudin aliquet et non nisi. Proin faucibus consectetur lorem, vitae cursus neque ultricies eu. Morbi lobortis ante vel tellus maximus, a semper elit accumsan. Sed nec enim aliquet, varius nisl eu, porta ex. Nulla facilisi. Mauris fringilla turpis eget diam placerat, ut sagittis lacus tristique. Vivamus scelerisque enim ut magna tristique, eu cursus mauris vulputate. Proin molestie sed lacus eu aliquam. Ut sagittis ipsum felis, vel elementum arcu dignissim nec. Vestibulum elementum vestibulum tincidunt. Donec iaculis accumsan iaculis. Fusce ipsum dui, convallis id imperdiet eget, posuere et lacus. Nam venenatis felis lorem, at sollicitudin est rutrum et.',
    ];
    $elements['p5'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Sed laoreet at lacus sed tempus. Sed dictum augue vitae condimentum gravida. Aenean vitae maximus augue. Phasellus eget tempus tellus, at iaculis odio. Aliquam nulla lacus, pellentesque eget est non, accumsan maximus ante. Nunc non sagittis quam. Cras tincidunt maximus metus eget tincidunt. Sed commodo mauris et convallis posuere. Cras commodo velit ut arcu fringilla, sed tincidunt eros tempus. Nulla sit amet leo quis dolor molestie tristique vel nec risus. Maecenas at ligula dui.',
    ];

    return $elements;
  }

}
