<?php

namespace Drupal\pdf_tools;

interface PDFAnalyserInterface {

  /**
   * Get the number of pages in a pdf.
   *
   * @param string $uri
   *
   * @return int
   */
  public function countPages($uri);

  /**
   * Get an array of page sizes
   *
   * @param $uri
   *
   * @return array
   *
   * @todo: Implement
  public function pageSizes($uri);
   */

  /**
   * Get the page size.
   *
   * @param $uri
   * @param $page_num
   *
   * @return array
   *
   * @todo: Implement
   *
  public function pageSize($uri, $page_num);
   */
}
