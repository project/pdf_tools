<?php

namespace Drupal\pdf_tools;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\pdf_tools\Entity\PDFStyle;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;

/**
 * Class PDFGenerationStyle
 *
 * Get options for generating pdfs.
 *
 * @package Drupal\pdf_tools
 */
class PDFGenerationStyle {

  /**
   * @var \Drupal\pdf_tools\Entity\PDFStyle|mixed|NULL
   */
  protected $pdfStyle;

  /**
   * @var array
   */
  protected $options;

  /**
   * @var array
   */
  protected $defaults;

  /**
   * PDFGenerationStyle constructor.
   *
   * @param array $defaults
   * @param array $options
   * @param \Drupal\pdf_tools\Entity\PDFStyle|NULL $pdf_style
   */
  public function __construct($defaults = [], $options = [], PDFStyle $pdf_style = NULL) {
    $this->defaults = $defaults;
    $this->options = $options;
    $this->pdfStyle = isset($options['pdf_style']) ? $options['pdf_style'] : $pdf_style;
  }

  /**
   * Get a pdf generation options.
   *
   * @param $key
   * @param null $default
   *
   * @return array|\Drupal\Core\TypedData\TypedDataInterface|mixed|null
   */
  public function getOption($key, $default = NULL) {
    if (isset($this->options[$key])) {
      return $this->options[$key];
    }

    // @todo: Special behaviour for boolean
    try {
      $field_name = str_replace('-', '_', $key);
      if (
        $this->pdfStyle
        && $this->pdfStyle->hasField($field_name)
        && !$this->pdfStyle->get($field_name)->isEmpty()
      ) {
        /** @var \Drupal\Core\Field\FieldItemBase $item */
        $item = $this->pdfStyle->get(str_replace('-', '_', $key))->get(0);

        if ($item instanceof FileItem) {
          return $item->entity->getFileUri();
        }
        else {
          if ($item instanceof EntityReferenceItem) {
            return $item->entity;
          }
          else {
            if ($item instanceof TextItemBase) {
              return $item->getValue();
            }
            else {
              return $item->get($item->mainPropertyName())->getValue();
            }
          }
        }
      }
    }
    catch (MissingDataException $e) {}

    if (isset($this->defaults[$key])) {
      return $this->defaults[$key];
    }

    return $default;
  }
}
