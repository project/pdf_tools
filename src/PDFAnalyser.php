<?php

namespace Drupal\pdf_tools;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class PDFAnalyser
 *
 * @package Drupal\pdf_tools
 */
abstract class PDFAnalyser implements PDFAnalyserInterface {

  /**
   * Filesystem
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * PDFAnalyser constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

}
