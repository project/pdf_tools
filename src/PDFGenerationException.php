<?php

namespace Drupal\pdf_tools;

/**
 * Class PDFGenerationException
 *
 * @package Drupal\pdf_tools
 */
class PDFGenerationException extends \Exception {}
