<?php

namespace Drupal\pdf_tools\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

class PDFStyleHtmlRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    if ($entity_type->hasLinkTemplate('duplicate-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('duplicate-form'));
      $route
        ->setDefaults([
          '_entity_form' => $entity_type_id.'.duplicate',
          '_title' => 'Duplicate PDF Style',
        ])
        ->setRequirement('_entity_access', $entity_type_id.'.duplicate')
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);
      $route->setRequirement($entity_type_id, '\d+');
      $collection->add('entity.'.$entity_type_id.'.duplicate_form', $route);
    }

    return $collection;
  }

}
