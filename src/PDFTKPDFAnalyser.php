<?php

namespace Drupal\pdf_tools;

class PDFTKPDFAnalyser extends PDFAnalyser {

  /**
   * Get the number of pages in a pdf.
   *
   * @param string $uri
   *
   * @return int
   */
  public function countPages($uri) {
    $real_path = $this->fileSystem->realpath($uri);

    $script = "pdftk \"{$real_path}\" dump_data | grep NumberOfPages | sed = 's/[^0-9]*//'";
    exec($script, $num);

    return reset($num);
  }

}
