<?php

namespace Drupal\pdf_tools;

interface PDFManipulatorInterface {

  /**
   * Stamp the stamp pdf onto all pages of the input pdf.
   *
   * @param string $pdf
   *   The URI of the pdf to have to the stamp applied to.
   * @param string $stamp_pdf
   *   The URI of the pdf to be stamped onto the input pdf.
   * @param string $output_pdf
   *   (Optional) The URI of the destination pdf.
   *
   * @return string
   *   The URI of the updated pdf.
   */
  public function stamp($pdf, $stamp_pdf, $output_pdf = NULL);

  /**
   * Stamp the stamp pdf onto all pages of the input pdf.
   *
   * @param string $pdf
   *   The URI of the pdf to have to the stamp applied to.
   * @param string $background_pdf
   *   The URI of the pdf to be stamped onto the input pdf.
   * @param string $output_pdf
   *   (Optional) The URI of the destination pdf.
   *
   * @return string
   *   The URI of the updated pdf.
   */
  public function background($pdf, $background_pdf, $output_pdf = NULL);
}
