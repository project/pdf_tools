<?php

namespace Drupal\pdf_tools;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;

class WKHTMLtoPDFGenerator extends PDFGeneratorBase {

  /**
   * Supported WKHTMLtoPDF Options.
   */
  protected $supportedOptions = [
    'T' => [],
    'R' => [],
    'B' => [],
    'L' => [],
    's' => [],
    'background' => [
      'type' => 'boolean',
      'map' => [
        0 => 'no-background',
        1 => 'background',
      ],
    ],
    'dpi' => [],
    'forms' => [
      'type' => 'boolean',
      'map' => [
        0 => 'disable-forms',
        1 => 'enable-forms',
      ],
    ],
    'grayscale' => [
      'type' => 'boolean',
      'map' => [
        1 => 'grayscale',
      ]
    ],
    'print-media-type' => [
      'type' => 'boolean',
      'map' => [
        1 => 'print-media-type',
      ],
    ],
    'margin-top' => [],
    'margin-bottom'  => [],
    'margin-left' => [],
    'margin-right' => [],
    'orientation' => [],
    'outline' => [
      'type' => 'boolean',
      'map' => [
        0 => 'no-outline',
        1 => 'outline',
      ],
    ],
    'outline-depth' => [],
    'page-size' => [],
    'page-width' => [
      'preppedby' => 'page-size',
    ],
    'page-height' => [
      'preppedby' => 'page-size',
    ],
    'smart-shrinking' => [
      'type' => 'boolean',
      'map' => [
        0 => 'disable-smart-shrinking',
        1 => 'enable-smart-shrinking',
      ]
    ],
    'header-type' => [],
    'header-elements' => [
      'preppedby' => 'header-type',
    ],
    'header-html' => [
      'preppedby' => 'header-type',
    ],
    'header-left' => [
      'preppedby' => 'header-type',
    ],
    'header-center' => [
      'preppedby' => 'header-type',
    ],
    'header-right' => [
      'preppedby' => 'header-type',
    ],
    'header-font-name' => [
      'preppedby' => 'header-type',
    ],
    'header-font-size' => [
      'preppedby' => 'header-type',
    ],
    'header-line' => [
      'type' => 'boolean',
      'map' => [
        0 => 'no-header-line',
        1 => 'header-line',
      ],
    ],
    'header-spacing' => [],
    'footer-type' => [],
    'footer-elements' => [
      'preppedby' => 'footer-type',
    ],
    'footer-html' => [
      'preppedby' => 'footer-type',
    ],
    'footer-left' => [
      'preppedby' => 'footer-type',
    ],
    'footer-center' => [
      'preppedby' => 'footer-type',
    ],
    'footer-right' => [
      'preppedby' => 'footer-type',
    ],
    'footer-font-name' => [
      'preppedby' => 'footer-type',
    ],
    'footer-font-size' => [
      'preppedby' => 'footer-type',
    ],
    'footer-line' => [
      'type' => 'boolean',
      'map' => [
        0 => 'no-header-line',
        1 => 'header-line',
      ],
    ],
    'footer-spacing' => [],
    'stamp-pdf' => [
      'noprep' => TRUE,
    ],
    'background-pdf' => [
      'noprep' => TRUE,
    ],
    'theme' => [
      'noprep' => TRUE,
    ]
  ];

  /**
   * {@inheritdoc}
   */
  public function generateFromFile($uri, array $options = array()) {
    return $this->generate(
      $this->fileSystem->realpath($uri),
      $options
    );
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromURL($url, array $options = array()) {
    return $this->generate($url, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromHTML($content, array $options = array()) {
    $in_file = $this->tempnamWithExtension('html', 'pdfhtml_');
    file_put_contents($in_file, $content);
    $return = $this->generateFromFile($in_file, $options);
    unlink($in_file);
    return $return;
  }

  /**
   * Generate a pdf
   *
   * @param $in_file
   * @param array $options
   *
   * @return null|string
   *
   * @throws \Drupal\pdf_tools\PDFGenerationException
   */
  protected function generate($in_file, array $options = array()) {
    $out_file = $this->getOutFile($options);
    $out_real_file = $this->fileSystem->realpath($out_file);

    if (substr($in_file, 0, 1) === '/') {
      file_put_contents(
        $in_file,
        $this->convertUrlsToAbsolute(file_get_contents($in_file))
      );
      $in_file = 'file://'.$in_file;
    }

    $script = "wkhtmltopdf ".$this->collapseOptions($this->prepareOptions($options))." \"{$in_file}\" \"{$out_real_file}\"";
    exec($script);

    if (file_exists($out_real_file) && (filesize($out_real_file) > 0)) {
      $this->postGenerate($out_file, $options, $in_file);
      return $out_file;
    }
    else {
      return NULL;
    }
  }

  /**
   * Apply stamping after generation
   *
   * @param $pdf
   * @param array $options
   * @param null $in_file   *
   */
  protected function postGenerate($pdf, array $options = array(), $in_file = NULL) {
    $style = new PDFGenerationStyle([], $options);

    if ($stamp_pdf = $style->getOption('stamp-pdf')) {
      if ($stamp_pdf instanceof File) {
        $stamp_pdf = $stamp_pdf->getFileUri();
      }

      if ($new_uri = $this->pdfManipulator->stamp($pdf, $stamp_pdf)) {
        $this->fileSystem->move($new_uri, $pdf, FileSystemInterface::EXISTS_REPLACE);
      }
    }

    if ($background_pdf = $style->getOption('background-pdf')) {
      if ($background_pdf instanceof File) {
        $background_pdf = $background_pdf->getFileUri();
      }

      if ($new_uri = $this->pdfManipulator->background($pdf, $background_pdf)) {
        $this->fileSystem->move($new_uri, $pdf, FileSystemInterface::EXISTS_REPLACE);
      }
    }
  }

  protected function collapseOptions(array $options) {
    return implode(' ', array_map(
      function($key, $value) {
        $return = ((strlen($key) > 1) ? '--' : '-').$key;
        if ($value) {
          $return .= ' '.$value;
        }
        return $return;
      },
      array_keys($options),
      array_values($options)
    ));
  }

  /**
   * Prepare the options.
   *
   * @param array $options
   *
   * @return array
   */
  protected function prepareOptions(array $options = array()) {
    $style = new PDFGenerationStyle([], $options);
    $prepped_options = [];

    foreach ($this->supportedOptions as $option => $option_info) {
      $option_info += [
        'type' => 'scalar',
      ];

      if (!empty($option_info['preppedby']) || !empty($option_info['noprep'])) {
        continue;
      }

      if (($value = $style->getOption($option)) !== NULL) {
        switch ($option_info['type']) {
          case 'boolean':
            if ($value && isset($option_info['map'][1])) {
              $prepped_options[$option_info['map'][1]] = '';
            }
            else if (!$value && isset($option_info['map'][0])) {
              $prepped_options[$option_info['map'][0]] = '';
            }
            break;
          case 'scalar':
          default:
            $prepped_options[$option] = $value;
        }

        // Page Size Options.
        if ($option === 'page-size') {
          if (
            (
              empty($style->getOption('page-size'))
              && $style->getOption('page-height')
              && $style->getOption('page-width')
            )
            || $style->getOption('page-size') === 'custom'
          ) {
            $prepped_options['page-height'] = $style->getOption('page-height');
            $prepped_options['page-width'] = $style->getOption('page-width');
          }
        }

        // Header and footer options.
        foreach (['header', 'footer'] as $hf) {
          unset($prepped_options[$hf.'-type']);

          $file_uri = NULL;
          switch ($style->getOption("{$hf}-type")) {
            case 'elements':
              $elements = $style->getOption("{$hf}-elements");
              $content = $this->renderElements($elements, $style);

            case 'html':
              $file_uri = $this->tempnamWithExtension('html', 'header');
              $file_uri = $this->fileSystem->realpath($file_uri);

              if (!isset($content)) {
                $content = $style->getOption("{$hf}-html");
              }
              $content = $this->convertUrlsToAbsolute($content);
              file_put_contents($file_uri, $content);
            case 'file':
              if (empty($file_uri)) {
                $file_opt = $style->getOption("{$hf}-file");
                if (is_string($file_opt)) {
                  $file_uri = $file_opt;
                }
                else {
                  $file_uri = $style->getOption("{$hf}-file")->getFileUri();
                }
              }

              $prepped_options["{$hf}-html"] = \Drupal::service('file_url_generator')->generateAbsoluteString($file_uri);
              break;
            case 'plain':
              foreach (['left', 'center', 'right'] as $pos) {
                if ($hf_plain = $style->getOption("{$hf}-{$pos}")) {
                  $prepped_options["{$hf}-{$pos}"] = $hf_plain;
                }
              }

              if ($hf_fs = $style->getOption("{$hf}-font-size")) {
                $prepped_options["{$hf}-font-size"] = $hf_fs;
              }

              if ($hf_fn = $style->getOption("{$hf}-font-name")) {
                $prepped_options["{$hf}-font-name"] = $hf_fn;
              }

              break;
          }
        }
      }
    }

    return $prepped_options;
  }
}
