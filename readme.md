# PDF Tools

PDF Tools is a drupal module to make it easier to manipulate PDF files. It provides service definitions and interfaces 
for PDF generation, manipulation, analysis and conversion. The default implementations of these services use local
command-line tools, contrib modules provide alternative implementations.

## Generation

The `pdf_tools.generator` service handles the generation of PDF's from html content. The default implementation requires
`wkhtmltopdf` is installed on the local machine. The `pdf_tools_docker` module provides an alternative implementation
utilising the wkhtmltopdf container available on the docker network.

### Example
```php
// Convert an entity to a PDF file.
$generator = \Drupal::service('pdf_tools.generator');
$pdf_uri = $generator->entityToPDF($entity_load, 'view_mode', $options);
```

Alternatively we can convert a URL to a pdf.
```php
// Convert an entity to a PDF file.
$generator = \Drupal::service('pdf_tools.generator');
$uri = $generator->generateFromUrl(
  Url::createFromRoute(
    'entity.node.canonical',
    [
      'node' => entity_load('node', 1),
    ],
    [
      'absolute' => TRUE,
    ]
  )->toString(),
  $options
);
``` 

## Analysis

The `pdf_tools.analyser` service provides methods that extract information from PDF files. The default implementation 
uses `pdftk` in the local command line.

### Example
```php
// Get the number of pages in the pdf document.
$analyser = \Drupal::service('pdf_tools.analyser');
$num_pages = $analyser->countPages('private://path/my.pdf');
```

## Manipulation

The `pdf_tools.manipulator` service provides methods than can manipulate pdfs. The default implementation uses `pdftk`
in the local command line.

### Example

```php
// Apply watermark to a pdf. $new_pdf coontains the URI of the input pdf file with the watermark applied.
// To override the original pdf, use \Drupal::service('file.system')->move().
$manipulator = \Drupal::service('pdf_tools.manipulator');
$new_pdf = $manipulator->background($pdf_uri, $watermark_pdf_uri);
```
