<?php

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\pdf_tools\Entity\PDFStyle;
use Drupal\pdf_tools\Entity\PDFStyleAccessControlHandler;
use Drupal\pdf_tools\Entity\PDFStyleListBuilder;
use Drupal\pdf_tools\Form\PDFStyleForm;
use Drupal\views\EntityViewsData;

/**
 * Install pdf_style entity.
 */
function pdf_tools_update_8001() {
  \Drupal::entityDefinitionUpdateManager()->installEntityType(
    new ContentEntityType([
      'id' => "pdf_style",
      'class' => PDFStyle::class,
      'label' => new TranslatableMarkup("PDF Style"),
      'label_singular' => new TranslatableMarkup("pdf style"),
      'label_plural' => new TranslatableMarkup("pdf styles"),
      'handlers' => [
        'storage' => SqlContentEntityStorage::class,
        'access"' => PDFStyleAccessControlHandler::class,
        'form' => [
          'default' => PDFStyleForm::class,
        ],
        'views_data' => EntityViewsData::class,
        'list_builder' => PDFStyleListBuilder::class,
        'route_provider' => [
          'html' => DefaultHtmlRouteProvider::class,
        ],
      ],
      'base_table' => "pdf_style",
      'revision_table' => "pdf_style_revision",
      'admin_permission' => "administer pdf styles",
      'entity_keys' => [
        'id' => "id",
        'revision' => "vid",
        'uuid' => "uuid",
        'owner' => "owner",
      ],
      'links' => [
        'collection' => "/admin/config/pdf/style",
        'canonical' => "/admin/config/pdf/style/{pdf_style}",
        'edit-form' => "/admin/config/pdf/style/{pdf-style}/edit",
        'add-form' => "/admin/config/pdf/style/add"
      ]
    ])
  );
}

/**
 * Install style label field.
 */
function pdf_tools_update_8002() {
  \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition(
    'label',
    'pdf_style',
    'pdf_tools',
    BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Label'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -100,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
  );
}
